#ifndef GAME_H
#define GAME_H

#include "player.h"
#include "board.h"
char end_game(board_t* board, player_t* p1, player_t* p2); 						   /* vérifie si la partie est terminée */
char get_rand_move(board_t* board);												   /* tirage du mouvement de l'algorithme aléatoire */
char get_rand_v2_move(board_t* board, player_t* player); 						   /* tirage du mouvement de l'algorithme aléatoire intelligent */
char get_glouton_move(board_t* board, player_t* player); 						   /* tirage du mouvement de l'algorithme glouton */
char get_glouton_prev_move(board_t* board, player_t* player);					   /* tirage du mouvement de l'algorithme glouton prévoyant */
char get_IA_move(player_t* player, board_t* board); 							   /* renvoie le mouvement d'une IA en fonction de son niveau */
char get_player_move(player_t* player, board_t* board); 						   /* renvoie un mouvement selon si le joueur est humain ou non */
double player_score_percent(board_t* board, player_t* player); 					   /* renvoie le score d'un joueur en pourcentage */
void print_board(board_t* board, player_t* p1, player_t* p2); 					   /* affiche le terrain */
void fill_board(board_t* board, player_t* p1, player_t* p2); 					   /* remplit le terrain aléatoirement */
void infect_near_cells(board_t* board, player_t* player, char move, int i, int j); /* modifie une case si besoin, puis passe aux cases adjacente (si la modification a eu lieu) */
void move_board_symbol(board_t* board, player_t* player, char move); 			   /* met à jour le terrain en fonction du coup d'un joueur */
void move_board(board_t* board, player_t* player); 								   /* demande le coup d'un joueur, et met le terrain à jour */
void tournament(board_t* board, player_t* p1, player_t* p2, int rounds);		   /* créer un tournoi d'IAs sur "rounds" tours */
void game(board_t* board, player_t* p1, player_t* p2);							   /* lance une partie */

#endif /* GAME_H */
