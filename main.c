#include "player.h"
#include "board.h"
#include "game.h"
#include "config.h"
#include <stdio.h> 
#include <time.h>
#include <stdlib.h>
#include <string.h>

int main(int argv, char** argc)
{
	srand(time(NULL));
	if ((argv > 1) && (argc[1][0] == '-') && (strlen(argc[1]) > 1))
	{
		if (argc[1][1] == 'r')
		{
			write_default_config();
			return 0;
		}
		else
		{
			print_help();
			return 0;
		}
	}

	int rounds;
	config_t* config;
	config = load_config();
	rounds = nb_round(config);
	player_t* p1 = create_player('^',P1_IA_config(config),P1_color_config(config));
	player_t* p2 = create_player('v',P2_IA_config(config),P2_color_config(config));	
	board_t* board = create_board(board_size_config(config),board_nbcolor_config(config));
	fill_board(board, p1, p2);
	free_config(config);
	
	if (rounds == 0)
	{	
		game(board, p1, p2);
	}
	else
	{
		tournament(board, p1, p2, rounds);
	}
	return 0;
}

