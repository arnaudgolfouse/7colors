#ifndef BOARD_H
#define BOARD_H

typedef struct board board_t;

board_t* copy_board(board_t* board);
board_t* create_board(int size, int nbcolor);
void free_board(board_t* board);
void print_color_cell(char cell);
int board_size(board_t* board);
int board_nbcolor(board_t* board);
char get_cell(board_t* board, int x, int y);
void set_cell(board_t* board, int x, int y, char color);

#endif /* BOARD_H */
