#include "board.h"
#include <stdio.h>    
#include <stdlib.h>	  
#include <time.h>

struct board 
{
	int nbcolor;
	int size;
	char* p;
};

board_t* copy_board(board_t* board)
{
	board_t* result = malloc(sizeof(board_t));
	result->nbcolor = board->nbcolor;
	result->size = board->size;
	result->p = malloc(board->size*board->size*sizeof(char));
	for (int i=0; i < board->size*board->size; i++)
	{
		result->p[i]=board->p[i];
	}
	return result;
}

board_t* create_board(int size, int nbcolor)
{
	board_t* result = malloc(sizeof(board_t));
	result->nbcolor = nbcolor;
	result->size = size;
	result->p = malloc(size*size*sizeof(char));
	return result;
}

void free_board(board_t* board)
{
	free(board->p);
	free(board);
}

int board_size(board_t* board)
{
	return board->size;
}

int board_nbcolor(board_t* board)
{
	return board->nbcolor;
}

char get_cell(board_t* board, int x, int y)
{
	return board->p[x + board_size(board) * y];
}

void set_cell(board_t* board, int x, int y, char color)
{
    	if ( ( x < 0 ) || ( y < 0 ) || ( x >= board_size(board) ) || ( y >= board_size(board) ) )
	{
		printf("\nset_cell out of bound\n");
	}
	else
	{
		board->p[y * board_size(board) + x] = color;
	}
}

void print_color_cell(char cell)
{
	char* red = "\x1b[31m";
	char* green = "\x1b[32m";
	char* yellow = "\x1b[33m";
	char* blue = "\x1b[34m";
	char* magenta = "\x1b[35m";
	char* cyan = "\x1b[36m";
	char* black = "\x1b[30m";
	char* white = "\x1b[37m";
	char* result=black;
	if (cell == 'A')
	{
		result = red;
	}
	if (cell == 'B')
	{
		result = blue;
	}
	if (cell == 'C')
	{
		result = green;
	}
	if (cell == 'D')
	{
		result = magenta;
	}
	if (cell == 'E')
	{
		result = cyan;
	}
	if (cell == 'F')
	{
		result = yellow;
	}
	if (cell == 'G')
	{
		result = white;
	}
	printf(result);
}
