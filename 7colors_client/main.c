#include "player.h"
#include "board.h"
#include "game.h"
#include "config.h"
#include "client_7colors.h"
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

int main(int argv, char** argc)
{
	srand(time(NULL));
	if ((argv > 1) && (argc[1][0] == '-') && (strlen(argc[1]) > 1))
	{
		if (argc[1][1] == 'r')
		{
			write_default_config();
			return 0;
		}
		else
		{
			print_help();
			return 0;
		}
	}

	config_t* config;
	config = load_config();
	
	char p_number;
	char p_color;
	char p_symbol;
	
	/// client initialization
	
	printf("\nconnecting to server...\n\n");
	
	client_t* client = create_client(server_IP_config(config), server_port_config(config), 5);
	while ((string_equal(client_getbuffer(client), "1") || string_equal(client_getbuffer(client), "2")) != 1)
	{
		client_setbuffer(client, "hello");
		client_send(client);
		client_recv(client);
	}
	
	player_t* p1 = create_player(1, P1_symbol_config(config), player_IA_config(config), P1_color_config(config));
	player_t* p2 = create_player(2, P2_symbol_config(config), player_IA_config(config), P2_color_config(config));
	
	if (client_getbuffer(client)[0] == '1')
	{
		player_setIA(p2, 5);
		p_number = '1';
		p_color = player_color(p1);
		p_symbol = player_symbol(p1);
	} else {
		player_setIA(p1, 5);
		p_number = '2';
		p_color = player_color(p2);
		p_symbol = player_symbol(p2);
	}
	
	if (p_number == '1')
	{
		print_color_player(p1);
	} else {
		print_color_player(p2);
	}
	printf("\nyou are player %c\n", p_number);
	print_end_color();
	
	char symbcol[4];
	symbcol[0] = p_symbol;
	symbcol[1] = p_color + '0';
	symbcol[2] = 0;
		
	client_setbuffsize(client, 10);
	if (string_equal(client_echange(client, p_number, symbcol), "received") != 1) { return -1; }
	strcpy(symbcol, client_echange(client, p_number, "symbcol"));
	
	player_setsymbol(p1, symbcol[0]);
	player_setcolor(p1, symbcol[1] - '0');
	player_setsymbol(p2, symbcol[2]);
	player_setcolor(p2, symbcol[3] - '0');
	
	printf("\ncreating board...\n");
	
	int boardsize = atoi(client_echange(client, p_number, "boardsize"));
	int boardnbcolor = atoi(client_echange(client, p_number, "nbcolor"));
	
	board_t* board = create_board(boardsize ,boardnbcolor);
	client_setbuffsize(client, boardsize*boardsize + 1);
	strcpy(board_data(board), client_echange(client, p_number, "board"));
	
	printf("\nstarting game...\n\n");
	
	client_setbuffsize(client, 4);
	
	game(board, p1, p2, client);
	
	client_sendfrom(client, p_number, "win");

	free_config(config);
	free_client(client);
	free_board(board);
	free_player(p1);
	free_player(p2);
	
	return 0;
}

