#ifndef PLAYER_H
#define PLAYER_H

typedef struct player player_t;

player_t* create_player(char number, char symbol, char I, char color);
void free_player(player_t* player);
char player_number(player_t* player);
char player_symbol(player_t* player);
char player_color(player_t* player);
char player_IA(player_t* player);
void player_setnumber(player_t* player, char number);
void player_setIA(player_t* player, char IA);
void player_setcolor(player_t* player, char color);
void player_setsymbol(player_t* player, char symbol);
void print_end_color();
void print_color_player(player_t* player);
void print_color_player_symbol(player_t* player);
int get_player_score(player_t* player);
void set_player_score(player_t* player, int score);
void add_player_score(player_t* player);
void switch_player(player_t** pp1, player_t** pp2);

#endif /* PLAYER_H */
