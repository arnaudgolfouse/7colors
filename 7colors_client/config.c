#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct config
{
	char player_IA;
	char P1_color;
	char P2_color;
	char P1_symbol;
	char P2_symbol;
	int board_size;
	int board_nbcolor;
	char* server_IP;
	char* server_port;
};

void free_config(config_t* config)
{
	free(config->server_IP);
	free(config->server_port);
	free(config);
}

char string_equal(char* str1, char* str2)
{
	unsigned int n;
	n = strlen(str1);
	if (n != strlen(str2))
	{
		return 0;
	}
	for(unsigned int i=0; i < n; i++)
	{
		if (str1[i] != str2[i])
		{
			return 0;
		}
	}
	return 1;
}	

config_t* load_config()
{
	config_t* result = malloc(sizeof(config_t));
	FILE* config_file;
	config_file = fopen("7colors.conf", "r");
	result->player_IA = 255;
	result->P1_color = 255;
	result->P2_color = 255;
	result->P1_symbol = 255;
	result->P2_symbol = 255;
	result->board_size = 0;
	result->board_nbcolor = 0;
	char* str = malloc(32*sizeof(char));
	while (fscanf(config_file, "%31s", str)  != EOF)
	{
		if (string_equal(str,"player_IA"))
		{
			fscanf(config_file, "%31s", str);
			if (string_equal(str,"="))
			{
				fscanf(config_file, "%31s", str);
				result->player_IA = str[0] - '0';
			}
		}
		if (string_equal(str,"P1_color"))
		{
			fscanf(config_file, "%31s", str);
			if (string_equal(str,"="))
			{
				fscanf(config_file, "%31s", str);
				result->P1_color = str[0] - '0';
			}
		}
		if (string_equal(str,"P2_color"))
		{
			fscanf(config_file, "%31s", str);
			if (string_equal(str,"="))
			{
				fscanf(config_file, "%31s", str);
				result->P2_color = str[0] - '0';
			}
		}
		if (string_equal(str,"P1_symbol"))
		{
			fscanf(config_file, "%31s", str);
			if (string_equal(str,"="))
			{
				fscanf(config_file, "%31s", str);
				result->P1_symbol = str[0];
			}
		}
		if (string_equal(str,"P2_symbol"))
		{
			fscanf(config_file, "%31s", str);
			if (string_equal(str,"="))
			{
				fscanf(config_file, "%31s", str);
				result->P2_symbol = str[0];
			}
		}
		if (string_equal(str, "server_IP"))
		{
			fscanf(config_file, "%31s", str);
			if (string_equal(str, "="))
			{
				fscanf(config_file, "%31s", str);
				result->server_IP = malloc(strlen(str));
				strcpy(result->server_IP, str);
			}
		}
		if (string_equal(str, "server_port"))
		{
			fscanf(config_file, "%31s", str);
			if (string_equal(str, "="))
			{
				fscanf(config_file, "%31s", str);
				result->server_port = malloc(strlen(str));
				strcpy(result->server_port, str);
			}
		}
	}
	free(str);
	fclose(config_file);
	
	return result;
}
	
void check_config(config_t* config)
{
	if (config->player_IA > 9)
	{
		printf("Unvalid value for player_IA in 7colors.conf\nUsing default value\n");
		config->player_IA = 0;
	}
	if (config->P1_color > 8)
	{
		printf("Unvalid value for P1_color in 7colors.conf\nUsing default value\n");
		config->P1_color = 1;
	}
	if (config->P2_color > 8)
	{
		printf("Unvalid value for P2_color in 7colors.conf\nUsing default value\n");
		config->P2_color = 2;
	}
	if ((config->board_size > 99) || (config->board_size < 1))
	{
		printf("Unvalid value for board_size in 7colors.conf\nUsing default value\n");
		config->board_size = 30;
	}
	if ((config->board_nbcolor > 8) || (config->board_nbcolor < 1))
	{
		printf("Unvalid value for nb_colors in 7colors.conf\nUsing default value\n");
		config->board_nbcolor = 7;
	}
	if (config->P1_color == config->P2_color)
	{
		printf("The two players can't have the same color\nUsing default colors\n");
		config->P1_color = 1;
		config->P2_color = 2;
	}
}

char player_IA_config(config_t* config)
{
	return config->player_IA;
}

char P1_color_config(config_t* config)
{
	return config->P1_color;
}

char P2_color_config(config_t* config)
{
	return config->P2_color;
}

char P1_symbol_config(config_t* config)
{
	return config->P1_symbol;
}

char P2_symbol_config(config_t* config)
{
	return config->P2_symbol;
}

int board_size_config(config_t* config)
{
	return config->board_size;
}

int board_nbcolor_config(config_t* config)
{
	return config->board_nbcolor;
}

char* server_IP_config(config_t* config)
{
	return config->server_IP;
}

char* server_port_config(config_t* config)
{
	return config->server_port;
}

void write_default_config()
{
	FILE* config_file;
	config_file = fopen("7colors.conf", "w");
	fclose(config_file);
	remove("7colors.conf");
	config_file = fopen("7colors.conf", "w");
	fprintf(config_file, "player_IA = 0\nP1_color = 1\nP2_color = 2\nP1_symbol = ^\nP2_symbol = v\nboard_size = 30\nnb_colors = 7\nserver_IP = 127.0.0.1\nserver_port = 5000");
	fclose(config_file);
	printf("7colors.conf has been reseted.\nModify it to change the options.\nUse -h or anything else than -r to get the help.\n");
}

void print_help()
{
	printf("You can change the options in the file 7colors.conf\n\nplayer_IA is the level of the IA (up to 4), use 0 if you want a human player.\n\nP1_color and P2_color are the colors of each player:\n1 red\n2 blue\n3 green\n4 magenta\n5 cyan\n6 yellow\n7 white\n8 black\n\nboard_size is the size of the board.\n\nnb_color is the number of colors (up to 8).\n\ntournament_round is the number of rounds if you are making an IA tournament, use 000 to turn OFF the tournament mode (the number must be 3 digits long).\n");
}
