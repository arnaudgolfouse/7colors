
#include "client_7colors.h"
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>

void wait(int t)
{
	time_t t1 = time(NULL);
	while ((time(NULL)-t1) < t) { }
}

struct client
{
	int socket_client;
	struct sockaddr_in address;
	int buff_size;
	char* buffer;
};

client_t* create_client(char* server_IP, char* server_port, int buff_size)
{
	client_t* result = malloc(sizeof(client_t));
	
	/// création de socket
	int socket_client = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	
	/// création de l'adresse
	struct sockaddr_in address;
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = inet_addr(server_IP);
	address.sin_port = htons(atoi(server_port));
	socklen_t addr_len = sizeof(address);
	
	/// connect
	if (connect(socket_client, (struct sockaddr*) &address, addr_len) == -1) {
		printf("ERREUR connect : errno %d\n", errno);
		return NULL;
	}
	
	result->socket_client = socket_client;
	result->address = address;
	
	/// buffer
	result->buff_size = buff_size;
	result->buffer = (char*) malloc(buff_size*sizeof(char));
	memset(&(result->buffer)[0], 0, result->buff_size);
	
	return result;
}

void free_client(client_t* client)
{
	close(client->socket_client);
	free(client->buffer);
	free(client);
}

int client_getsocket(client_t* client)
{
	return client->socket_client;
}

struct sockaddr_in client_getaddress(client_t* client)
{
	return client->address;
}

int client_getbuff_size(client_t* client)
{
	return client->buff_size;
}

char* client_getbuffer(client_t* client)
{
	return client->buffer;
}

void client_clearbuff(client_t* client)
{
	memset(&(client->buffer)[0], 0, client->buff_size + 1);
}

void client_setbuffsize(client_t* client, int size)
{
	client->buff_size = size;
	client->buffer = (char*) realloc(client->buffer, (client->buff_size)*sizeof(char));
}

void client_setbuffer(client_t* client, char* str)
{
	if (((int) strlen(str)) > client->buff_size)
	{
		client_setbuffsize(client, (int) strlen(str));
	}
	client_clearbuff(client);
	strcpy(client->buffer, str);
}

/* connect the socket to the server */
int client_connect(client_t* client)
{
	close(client->socket_client);
	client->socket_client = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	struct sockaddr_in addr = client_getaddress(client);
	socklen_t addr_len = sizeof(addr);
	if (connect(client_getsocket(client), (struct sockaddr*) &addr, addr_len) == -1)
	{
		printf("ERREUR connect : errno %d\n", errno);
		return -1;
	}
	return 0;
}

/* receive a message from the server : if successfull, return 0, else return -1 and print errno */
int client_recv(client_t* client)
{
	wait(1);
	client_clearbuff(client);
	if (recv(client->socket_client, client->buffer, client->buff_size, 0) == -1) {
		printf("ERREUR recv : errno %d\n", errno);
		return -1;
	}
	return 0;
}

/* send a message to the server : if successfull, return 0, else return -1 and print errno */
int client_send(client_t* client)
{
	client_connect(client);
	wait(1);
	if (send(client->socket_client, client->buffer, client->buff_size, 0) == -1) {
		printf("ERREUR send : errno %d\n", errno);
		return -1;
	}
	return 0;
}

int client_sendfrom(client_t* client, char p_number, char* message)
{
	char* result = malloc(1 + strlen(message));
	memset(&(result)[0], 0, 2 + strlen(message));
	result[0] = p_number;
	strcat(result, message);
	client_setbuffer(client, result);
	free(result);
	return client_send(client);
}

char* client_recvfrom(client_t* client, char p_number)
{
	client_recv(client);
	while (client_getbuffer(client)[0] != p_number)
	{
		client_recv(client);
	}
	return client_getbuffer(client) + 1;
}

char* client_echange(client_t* client, char p_number, char* message)
{
	char* result = malloc(1 + strlen(message));
	memset(&(result)[0], 0, 2 + strlen(message));
	result[0] = p_number;
	strcat(result, message);
	client_clearbuff(client);
	while (client_getbuffer(client)[0] != p_number)
	{
		client_setbuffer(client, result);
		client_send(client);
		client_recv(client);
	}
	free(result);
	return client_getbuffer(client) + 1;
}
