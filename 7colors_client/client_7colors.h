#ifndef CLIENT_7COLORS_H
#define CLIENT_7COLORS_H

#include "config.h"

typedef struct client client_t;

client_t* create_client(char* server_IP, char* server_port, int buff_size);
void free_client(client_t* client);
int client_getsocket(client_t* client);
struct sockaddr_in client_getaddress(client_t* client);
int client_getbuff_size(client_t* client);
char* client_getbuffer(client_t* client);
void client_clearbuff(client_t* client);
void client_setbuffsize(client_t* client, int size);
void client_setbuffer(client_t* client, char* str);
int client_connect(client_t* client);
int client_recv(client_t* client);
int client_send(client_t* client);
int client_sendfrom(client_t* client, char p_number, char* message);
char* client_recvfrom(client_t* client, char p_number);
char* client_echange(client_t* client, char p_number, char* message);

#endif /* CLIENT_7COLORS_H */
