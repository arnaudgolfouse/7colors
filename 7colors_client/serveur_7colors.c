


#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>


int main(int argc, char** argv)
{
	printf("hello !\n");
	/// création de socket
	int socket_serveur = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	
	/// création de l'adresse
	struct sockaddr_in adresse;
	adresse.sin_family = AF_INET;
	adresse.sin_addr.s_addr = INADDR_ANY;
	socklen_t addr_len = sizeof(adresse);
	
	/// bind (port spécifié en argument)
	adresse.sin_port = htons(atoi(argv[1]));
	if (bind(socket_serveur, (struct sockaddr*) &adresse, addr_len) < 0) {
		printf("ERREUR bind : erreur No %d\n", errno);
		return -1;
	}
	
	/// listen (SOMAXCONN connections max)
	if (listen(socket_serveur, SOMAXCONN) == -1) {
		printf("ERREUR listen : erreur No %d\n", errno);
		return -1;
	}
	
	int socket_local;
	char buff[64];
	while (1) {
		/// accept
		socket_local = accept(socket_serveur, (struct sockaddr*) &adresse, &addr_len);
		if (socket_local == -1) {
			printf("ERREUR accept : erreur No %d\n", errno);
			return -1;
		}
		
		/// recv
		memset(&buff[0], 0, sizeof(buff));
		if (recv(socket_local, buff, sizeof(buff), 0) == -1) {
			printf("ERREUR recv : erreur No %d\n", errno);
			return -1;
		} else {
			printf("received \"%s\"", buff);
		}
		
		/// send
		if (send(socket_local, buff, sizeof(buff), 0) == -1) {
			printf("ERREUR send : erreur No %d\n", errno);
			return -1;
		} else {
			printf(", returned \"%s\".\n", buff);
		}
	}
		
	return 0;
}
