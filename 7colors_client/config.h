#ifndef CONFIG_H
#define CONFIG_H

typedef struct config config_t;

char string_equal(char* str1, char* str2);
config_t* load_config();
void check_config(config_t* config);
void free_config(config_t* config);
char player_IA_config(config_t* config);
char P1_color_config(config_t* config);
char P2_color_config(config_t* config);
char P1_symbol_config(config_t* config);
char P2_symbol_config(config_t* config);
int board_size_config(config_t* config);
int board_nbcolor_config(config_t* config);
char* server_IP_config(config_t* config);
char* server_port_config(config_t* config);
void write_default_config();
void write_readme();
void print_help();

#endif /* CONFIG_H */
