project(7colors)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -Wextra -Wno-unused-parameter -Wno-unused-function")
add_executable(7colors main.c config.c config.h game.c game.h board.c board.h player.c player.h client_7colors.c client_7colors.h)
