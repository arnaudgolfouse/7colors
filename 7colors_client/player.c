#include "player.h"
#include <stdio.h>
#include <stdlib.h>

struct player
{
	char number;
	char IA;
	char symbol;
	char color;
	int score;
};

player_t* create_player(char number, char symbol, char IA, char color)
{
	player_t* result = malloc(sizeof(player_t));
	result->number = number;
	result->IA = IA;
	result->symbol = symbol;
	result->color = color;
	result->score = 1;
	return result;
}

void free_player(player_t* player)
{
	free(player);
}

char player_number(player_t* player)
{
	return player->number;
}

char player_IA(player_t* player)
{
	return player->IA;
}

char player_symbol(player_t* player)
{
	return player->symbol;
}

void player_setnumber(player_t* player, char number)
{
	player->number = number;
}

void player_setIA(player_t* player, char IA)
{
	player->IA = IA;
}

void player_setcolor(player_t* player, char color)
{
	player->color = color;
}

void player_setsymbol(player_t* player, char symbol)
{
	player->symbol = symbol;
}

char player_color(player_t* player)
{
	return player->color;
}

void print_end_color()
{
	printf("\x1b[0m");
}

void print_color_player_symbol(player_t* player)
{
	char* red="\x1b[41m";
	char* blue="\x1b[44m";
	char* green="\x1b[42m";
	char* yellow="\x1b[43m";
	char* magenta="\x1b[45m";
	char* cyan="\x1b[46m";
	char* white="\x1b[47m";
	char* black="\x1b[40m";
	char* result=black;
	if ( player_color(player) == 2)
	{
		result=blue;	
	}
	if ( player_color(player) == 3)
	{
		result=green;	
	}
	if ( player_color(player) == 4)
	{
		result=magenta;	
	}
	if ( player_color(player) == 5)
	{
		result=cyan;	
	}
	if ( player_color(player) == 6)
	{
		result=yellow;	
	}
	if ( player_color(player) == 7)
	{
		result=white;	
	}
	if ( player_color(player) == 8)
	{
		result=black;	
	}
	if ( player_color(player) == 1)
	{
		result=red;	
	}
	printf("%s", result);
}

void print_color_player(player_t* player)
{
	char* red="\x1b[31m";
	char* blue="\x1b[34m";
	char* green="\x1b[32m";
	char* yellow="\x1b[33m";
	char* magenta="\x1b[35m";
	char* cyan="\x1b[36m";
	char* white="\x1b[37m";
	char* black="\x1b[30m";
	char* result=black;
	if ( player_color(player) == 2)
	{
		result=blue;	
	}
	if ( player_color(player) == 3)
	{
		result=green;	
	}
	if ( player_color(player) == 4)
	{
		result=magenta;	
	}
	if ( player_color(player) == 5)
	{
		result=cyan;	
	}
	if ( player_color(player) == 6)
	{
		result=yellow;	
	}
	if ( player_color(player) == 7)
	{
		result=white;	
	}
	if ( player_color(player) == 8)
	{
		result=black;	
	}
	if ( player_color(player) == 1)
	{
		result=red;	
	}
	printf("%s", result);
}

int get_player_score(player_t* player)
{
	return player->score;
}

void set_player_score(player_t* player, int score)
{
	player->score = score;
}

void add_player_score(player_t* player)
{
	player->score ++;
}

void switch_player(player_t** pp1, player_t** pp2)
{
	player_t* temp=*pp1;
	*pp1 = *pp2;
	*pp2 = temp;
}
