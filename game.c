#include "game.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/* vérifie si la partie est terminée */
char end_game(board_t* board, player_t* p1, player_t* p2)
{
	int s1=get_player_score(p1);
	int s2=get_player_score(p2);
	int n=board_size(board)*board_size(board);
	if ( 2*s1 > n )
	{
		return 1;
	}
	else if ( 2*s2 > n )
	{
		return 2;
	}
	else if ( s1 + s2 == n)
	{
		return 3;
	}
	else
	{
		return 0;
	}
}

/* tirage du mouvement de l'algorithme aléatoire */
char get_rand_move(board_t* board)
{
	char result;
	result = ( rand() % board_nbcolor(board) ) + 65;
	return result;
}

/* tirage du mouvement de l'algorithme aléatoire intelligent */
char get_rand_v2_move(board_t* board, player_t* player)
{
	char is_color_near[board_nbcolor(board)];
	for (int i=0; i<board_nbcolor(board); i++)
	{
		is_color_near[i]=0;
	}
	for (int i = 0; i < board_size(board); i++)
      	{
		for (int j = 0; j < board_size(board); j++)
	       	{
			if ( get_cell(board, i, j) == player_symbol(player) )
			{
				int cell;
				if ( i > 0 )
				{
					cell = (int) get_cell(board, i-1, j) - 65;
					if ( ( cell >= 0 ) && ( cell < board_nbcolor(board) ) )
					{
						is_color_near[cell]=1;
					}
				}
				if ( j > 0 )
				{
					cell = (int) get_cell(board, i, j-1) - 65;
					if ( ( cell >= 0 ) && ( cell < board_nbcolor(board) ) )
					{
						is_color_near[cell]=1;
					}
				}
				if ( i < board_size(board)-1 )
				{
					cell = (int) get_cell(board, i+1, j) - 65;
					if ( ( cell >= 0 ) && ( cell < board_nbcolor(board) ) )
					{
						is_color_near[cell]=1;
					}
				}
				if ( j < board_size(board)-1 )
				{
					cell = (int) get_cell(board, i, j+1) - 65;
					if ( ( cell >= 0 ) && ( cell < board_nbcolor(board) ) )
					{
						is_color_near[cell]=1;
					}
				}
			}
       		}
    	}
	int nbnear_color=0;
	for (int i=0; i<board_nbcolor(board); i++)
	{
		nbnear_color = nbnear_color + is_color_near[i];
	}
	if (nbnear_color == 0)
	{
		return 'A';
	}
	char near_color[nbnear_color];
	int near_color_pos=0;
	for (int color=0; color<board_nbcolor(board); color++)
	{
		if ( is_color_near[color] )
		{
			near_color[near_color_pos] = color + 65;
			near_color_pos++;
		}
	}
	int rng;
	rng = ( rand() % nbnear_color );
	return near_color[rng];
}

/* tirage du mouvement de l'algorithme glouton */
char get_glouton_move(board_t* board, player_t* player)
{
	board_t* copy;
	char move;
	char result;
	result = 'A';
	int score;
	score = get_player_score(player);
	int max_score;
	max_score = 0;
	for (int i=0; i < board_nbcolor(board); i++)
	{
		copy = copy_board(board);
		move = (char) i + 65;
		move_board_symbol(copy, player, move);
		if (max_score < get_player_score(player))
		{
			max_score = get_player_score(player);
			result = move;
		}
		free(copy);
		set_player_score(player, score);
	}
	return result;
}

/* tirage du mouvement de l'algorithme glouton prévoyant */
char get_glouton_prev_move(board_t* board, player_t* player)
{
	board_t* copy;
	board_t* copy2;
	char move;
	char move2;
	char result;
	result = 'A';
	int score;
	score = get_player_score(player);
	int score2;
	int max_score;
	max_score = 0;
	for (int i=0; i < board_nbcolor(board); i++)
	{
		copy = copy_board(board);
		move = (char) i + 65;
		move_board_symbol(copy, player, move);
		score2 = get_player_score(player);
		for (int j=0; j < board_nbcolor(board); j++)
		{
			copy2 = copy_board(copy);
			move2 = (char) j + 65;
			move_board_symbol(copy2, player, move2);
			if (max_score < get_player_score(player))
			{
				max_score = get_player_score(player);
				result = move;
			}
			free(copy2);
			set_player_score(player, score2);	
		}
		free(copy);
		set_player_score(player, score);
	}
	return result;
}

/* renvoie le mouvement d'une IA en fonction de son niveau */
char get_IA_move(player_t* player, board_t* board)
{
	char result='A';
	if ( is_player_IA(player) == 1 )
	{
		result = get_rand_move(board);
	}
	if ( is_player_IA(player) == 2 )
	{
		result = get_rand_v2_move(board, player);
	}
	if ( is_player_IA(player) == 3 )
	{
		result = get_glouton_move(board, player);
	}
	if ( is_player_IA(player) == 4 )
	{
		result = get_glouton_prev_move(board, player);
	}
	return result;
}

/* renvoie un mouvement selon si le joueur est humain ou non */
char get_player_move(player_t* player, board_t* board)
{
	if ( is_player_IA(player) )
	{
		char result;
		result = get_IA_move(player, board);
		printf("The IA has chosen %c.\nPress <ENTER> to continue.\n", result);
		char c;
		while ( (c=getchar()) != '\n' ) {}
		return result;
	}
	else
	{
		printf("Choose a letter:\n");
		char* data=malloc(32*sizeof(char));
		char c;
		scanf("%31s", data);
		while ( (c=getchar()) !='\n' ) {}
		return data[0];
	}
}

/* renvoie le score d'un joueur en pourcentage */
double player_score_percent(board_t* board, player_t* player)
{
	double result;
	result = get_player_score(player)*100;
	result = result / (board_size(board)*board_size(board));
	return result;
}

/* affiche le terrain */
void print_board(board_t* board, player_t* p1, player_t* p2)
{
	printf("Current board state:\n");
	for (int i = 0; i < board_size(board); i++)
       	{
        	for (int j = 0; j < board_size(board); j++)
	       	{
			char cell = get_cell(board, i, j);
			if ( cell == player_symbol(p1) )
			{
				print_color_player_symbol(p1);
				print_color_player(p1);
				printf("%c", cell);
				if (j < board_size(board) - 1)
				{
					if ((get_cell(board, i, j+1) == player_symbol(p1)) || (get_cell(board, i, j+1) == player_symbol(p2)))
					{
						printf(" ");
						print_end_color();
					}
					else
					{
						print_end_color();
						printf(" ");
					}	
				}
				else
				{
					print_end_color();
				}
			}
			else if ( cell == player_symbol(p2) )
			{	
				print_color_player_symbol(p2);
				print_color_player(p2);
				printf("%c", cell);
				if (j < board_size(board) - 1)
				{
					if ((get_cell(board, i, j+1) == player_symbol(p1)) || (get_cell(board, i, j+1) == player_symbol(p2)))
					{
						printf(" ");
						print_end_color();
					}
					else
					{
						print_end_color();
						printf(" ");
					}	
				}
				else
				{
					print_end_color();
				}
			}
			else
			{
				print_color_cell(cell);
				printf("%c ", cell);
				print_end_color();
			}
 	       }
        printf("\n");
   	}
	printf("\n");
	print_color_player(p1);
	printf("P1 score : %.2f %%", player_score_percent(board, p1));
	print_end_color();
	printf("\n");
	print_color_player(p2);
	printf("P2 score : %.2f %%", player_score_percent(board, p2));
	print_end_color();
	printf("\n\n");
}

/* remplit le terrain aléatoirement */
void fill_board(board_t* board, player_t* p1, player_t* p2)
{
	char color;
	for (int i = 0; i < board_size(board); i++)
       	{
		for (int j = 0; j < board_size(board); j++)
	       	{
			color = ( rand() % board_nbcolor(board) ) + 65;
			set_cell(board, i, j, color);
       		}
    	}
    	set_cell(board, 0, board_size(board) -1, player_symbol(p1));
	set_cell(board, board_size(board) -1, 0, player_symbol(p2));
}

/* modifie une case si besoin, puis passe aux cases adjacente (si la modification a eu lieu) */
void infect_near_cells(board_t* board, player_t* player, char move, int i, int j)
{
	if ( get_cell(board, i, j) == move )
	{
		set_cell(board, i, j, player_symbol(player));
		add_player_score(player);
		if (i > 0)
		{
			infect_near_cells(board, player, move, i-1, j);
		}
		if (j > 0)
		{
			infect_near_cells(board, player, move, i, j-1);
		}
		if (i < board_size(board)-1)
		{
			infect_near_cells(board, player, move, i+1, j);
		}
		if (j < board_size(board)-1)
		{
			infect_near_cells(board, player, move, i, j+1);
		}
	}
}

/* met à jour le terrain en fonction du coup d'un joueur */
void move_board_symbol(board_t* board, player_t* player, char move)
{
	for (int i = 0; i < board_size(board); i++)
      	{
		for (int j = 0; j < board_size(board); j++)
	       	{
			if ( get_cell(board, i, j) == player_symbol(player) )
			{
				if (i > 0)
				{
					infect_near_cells(board, player, move, i-1, j);
				}
				if (j > 0)
				{
					infect_near_cells(board, player, move,i , j-1);
				}
				if (i < board_size(board)-1)
				{
					infect_near_cells(board, player, move, i+1, j);
				}
				if (j < board_size(board)-1)
				{
					infect_near_cells(board, player, move, i, j+1);
				}
			}
       		}
	}
}

/* demande le coup d'un joueur, et met le terrain à jour */
void move_board(board_t* board, player_t* player)
{
	char move=get_player_move(player, board);
	while ( ( move-65 >= board_nbcolor(board) ) || ( move-65 < 0 ) )
	{
		printf("Invalid move!\n");
		move = get_player_move(player, board);
	}
	move_board_symbol( board, player, move);
}

/* créer un tournoi d'IAs sur "rounds" tours */
void tournament(board_t* board, player_t* p1, player_t* p2, int rounds)
{
	int score1=0;
	int score2=0;
	char rd_switch=0;
	char end=0;
	printf("TOURNAMENT MODE\n");
	for (int i=0; i < rounds; i++)
	{
		rd_switch = (rand() % 2);
		if (rd_switch)
		{
			switch_player(&p1, &p2);
		}
		while (end == 0)
		{
			move_board_symbol(board, p1, get_IA_move(p1, board));
			move_board_symbol(board, p2, get_IA_move(p2, board));
			end = end_game(board, p1, p2);
		}
		if (rd_switch)
		{
			switch_player(&p1, &p2);
		}
		if (get_player_score(p1) > get_player_score(p2))
		{
			score1 = score1 + 1;
		}
		if (get_player_score(p2) > get_player_score(p1))
		{
			score2 = score2 + 1;
		}
		end = 0;
		fill_board(board, p1, p2);
		set_player_score(p1, 1);
		set_player_score(p2, 1);
	}
	printf("\n");
	if (score1 > score2)
	{
		print_color_player(p1);
		printf("P1 WIN\n");
		print_end_color();
	}
	if (score2 > score1)
	{
		print_color_player(p2);
		printf("P2 WIN\n");
		print_end_color();
	}
	print_color_player(p1);
	printf("P1 Score: %d\n", score1);
	print_end_color();
	print_color_player(p2);
	printf("P2 Score: %d\n", score2);
	print_end_color();
}

/* lance une partie */
void game(board_t* board, player_t* p1, player_t* p2)
{
	printf("\n\nWelcome to the 7 wonders of the world of the 7 colors\n"
	"*****************************************************\n\n");
	print_board(board, p1, p2);
	char end=0;
	while (end == 0)
	{
		print_color_player(p1);
		printf("P1");
		print_end_color();
	       	printf(" turn to play.\n");
		move_board(board, p1);
		print_board(board, p1, p2);
		print_color_player(p2);
		printf("P2");
		print_end_color();
	       	printf(" turn to play.\n");
		move_board(board, p2);
		print_board(board, p1, p2);
		end = end_game(board, p1, p2);
		if (end == 1)
		{
			printf("\n");
			print_color_player(p1);
			printf("P1");
			print_end_color();
			printf(" WIN\n");
		}
		if (end == 2)
		{
			printf("\n");
			print_color_player(p2);
			printf("P2");
			print_end_color();
		       	printf(" WIN\n");
		}
		if (end == 3)
		{
			printf("\n");
			printf("\nIt's a tie !\n");
		}
	}
	free_player(p1);
	free_player(p2);
	free_board(board);
}
