#ifndef PLAYER_H
#define PLAYER_H

typedef struct player player_t;

player_t* create_player(char symbol, char I, char color);
void free_player(player_t* player);
char player_symbol(player_t* player);
char player_color(player_t* player);
char is_player_IA(player_t* player);
void print_end_color();
void print_color_player(player_t* player);
void print_color_player_symbol(player_t* player);
int get_player_score(player_t* player);
void set_player_score(player_t* player, int score);
void add_player_score(player_t* player);
void switch_player(player_t** pp1, player_t** pp2);

#endif /* PLAYER_H */
