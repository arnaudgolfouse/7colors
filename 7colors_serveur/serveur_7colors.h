#ifndef SERVEUR_7COLORS_H
#define SERVEUR_7COLORS_H

#include "config.h"

typedef struct server server_t;

server_t* create_server(int port, int buff_size);
void free_server(server_t* server);
char* server_getbuffer(server_t* server);
int server_getport(server_t* server);
int server_getbuffsize(server_t* server);
void server_clearbuff(server_t* server);
void server_setbuffsize(server_t* server, int size);
void server_setbuffer(server_t* server, char* str);
int server_accept(server_t* server);
int server_recv(server_t* server); // careful, erases any previous data in the buffer !
int server_send(server_t* server);
void server_waitfor(server_t* server, char* str);
void server_sendto(server_t* server, char player_no, char* message);
char* server_recvfrom(server_t* server, char player_no);

#endif /* SERVEUR_7COLORS_H */
