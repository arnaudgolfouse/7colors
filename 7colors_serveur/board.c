#include "board.h"
#include <stdio.h>    
#include <stdlib.h>	  
#include <time.h>

struct board 
{
	int nbcolor;
	int size;
	char* p;
};

board_t* create_board(int size, int nbcolor)
{
	board_t* result = malloc(sizeof(board_t));
	result->nbcolor = nbcolor;
	result->size = size;
	result->p = malloc(size*size*sizeof(char));
	return result;
}

void free_board(board_t* board)
{
	free(board->p);
	free(board);
}

int board_size(board_t* board)
{
	return board->size;
}

int board_nbcolor(board_t* board)
{
	return board->nbcolor;
}

char* board_data(board_t* board)
{
	return board->p;
}

void set_cell(board_t* board, int x, int y, char color)
{
    	if ( ( x < 0 ) || ( y < 0 ) || ( x >= board_size(board) ) || ( y >= board_size(board) ) )
	{
		printf("\nset_cell out of bound\n");
	}
	else
	{
		board->p[y * board_size(board) + x] = color;
	}
}

/* remplit le terrain aléatoirement */
void fill_board(board_t* board, char player1_symbol, char player2_symbol)
{
	char color;
	for (int i = 0; i < board_size(board); i++)
       	{
		for (int j = 0; j < board_size(board); j++)
	       	{
			color = ( rand() % board_nbcolor(board) ) + 65;
			set_cell(board, i, j, color);
       		}
    	}
    	set_cell(board, 0, board_size(board) -1, player1_symbol);
	set_cell(board, board_size(board) -1, 0, player2_symbol);
}
