
#include "serveur_7colors.h"
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

struct server
{
	int socket_server;
	int socket_local;
	struct sockaddr_in address;
	socklen_t addr_len;
	int buff_size;
	char* buffer;
};

server_t* create_server(int port, int buff_size)
{
	server_t* result = (server_t*) malloc(sizeof(server_t));
	
	/// socket creation
	
	int socket_server = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	
	/// address creation
	
	struct sockaddr_in address;
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	socklen_t addr_len = sizeof(address);
	
	/// bind
	
	address.sin_port = htons(port);
	if (bind(socket_server, (struct sockaddr*) &address, addr_len) < 0) {
		printf("ERREUR bind : errno %d\n", errno);
		return (server_t*) -1;
	}
	
	/// listen (SOMAXCONN connections max)
	
	if (listen(socket_server, SOMAXCONN) == -1) {
		printf("ERREUR listen : errno %d\n", errno);
		return (server_t*) -1;
	}
	
	result->socket_server = socket_server;
	result->socket_local = -1; // arbitrary initialization
	result->address = address;
	result->addr_len = addr_len;
	
	/// buffer
	
	result->buff_size = buff_size;
	result->buffer = malloc(buff_size*sizeof(char));
	memset(&(result->buffer)[0], 0, buff_size);
	
	return result;
}

void free_server(server_t* server)
{
	close(server->socket_server);
	close(server->socket_local);
	free(server->buffer);
	free(server);
}

char* server_getbuffer(server_t* server)
{
	return server->buffer;
}

int server_getport(server_t* server)
{
	return htons((server->address).sin_port);
}

int server_getbuffsize(server_t* server)
{
	return server->buff_size;
}

void server_clearbuff(server_t* server)
{
	memset(&(server->buffer)[0], 0, server->buff_size + 1);
}

void server_setbuffsize(server_t* server, int size)
{
	server->buff_size = size;
	server->buffer = (char*) realloc(server->buffer, (server->buff_size)*sizeof(char));
}

void server_setbuffer(server_t* server, char* str)
{
	if (((int) strlen(str)) > server->buff_size)
	{
		server_setbuffsize(server, (int) strlen(str));
	}
	server_clearbuff(server);
	strcpy(server->buffer, str);
}

int server_accept(server_t* server)
{
	server->socket_local = accept(server->socket_server, (struct sockaddr*) &(server->address), &(server->addr_len));
	if (server->socket_local == -1) {
		printf("Erreur accept : errno %d\n", errno);
		return -1;
	}
	return 0;
}

int server_recv(server_t* server) // careful, erases any previous data in the buffer !
{
	server_clearbuff(server);
	if (recv(server->socket_local, server->buffer, server->buff_size, 0) == -1) {
		printf("Erreur recv : errno %d\n", errno);
		return -1;
	} else {
		if (string_equal(server->buffer, "") != 1)
		{
			printf("received \"%s\"\n", server->buffer);
		}
		return 0;
	}
}

int server_send(server_t* server)
{
	if (send(server->socket_local, server->buffer, server->buff_size, 0) == -1) {
		return -1;
	} else {
		printf("sent \"%s\"\n", server->buffer);
		if (server_accept(server) == -1)
		{
			return -1;
		}
		return 0;
	}
}

void server_waitfor(server_t* server, char* str)
{
	server_setbuffsize(server, strlen(str));
	server_clearbuff(server);
	server_recv(server);
	while (string_equal(server_getbuffer(server), str) != 1)
	{
		server_setbuffer(server, "-1");
		server_send(server);
		server_recv(server);
	}
}

void server_sendto(server_t* server, char player_no, char* message)
{
	server_setbuffsize(server, 1 + strlen(message));
	server_clearbuff(server);
	char no[1];
	no[0] = player_no;
	server_setbuffer(server, no);
	strcat(server_getbuffer(server), message);
	server_send(server);
}

char* server_recvfrom(server_t* server, char player_no)
{
	server_recv(server);
	while (server_getbuffer(server)[0] != player_no)
	{
		server_setbuffer(server, "-1");
		server_send(server);
		server_recv(server);
	}
	return (server_getbuffer(server)+1);
}
