#ifndef CONFIG_H
#define CONFIG_H

typedef struct config config_t;

config_t* load_config();
void free_config(config_t* config);
char string_equal(char* str1, char* str2);
char P1_IA_config(config_t* config);
char P2_IA_config(config_t* config);
char P1_color_config(config_t* config);
char P2_color_config(config_t* config);
int board_size_config(config_t* config);
int board_nbcolor_config(config_t* config);
int nb_round(config_t* config);
int port_config(config_t* config);
void write_default_config();
void write_readme();
void print_help();

#endif /* CONFIG_H */
