#include "board.h"
#include "config.h"
#include "serveur_7colors.h"
#include <stdio.h> 
#include <time.h>
#include <stdlib.h>
#include <string.h>

int main(int argv, char** argc)
{
	srand(time(NULL));
	if ((argv > 1) && (argc[1][0] == '-') && (strlen(argc[1]) > 1))
	{
		if (argc[1][1] == 'r')
		{
			write_default_config();
			return 0;
		}
		else
		{
			print_help();
			return 0;
		}
	}

	config_t* config;
	config = load_config();
	if (nb_round(config) != 0)
	{	
		printf("ERREUR DE CONFIGURATION : mode tournoi pas encore développé !");
	}
	
	/// game data initialization
	
	printf("\ngame data initialization...\n");
	
	board_t* board = create_board(board_size_config(config),board_nbcolor_config(config));
	char str_board_size[12];
	sprintf(str_board_size, "%d", board_size(board)); // on aura besoin de la taille sous forme char*
	char str_nb_color[12];
	sprintf(str_nb_color, "%d", board_nbcolor(board)); // on aura besoin du nombre de couleurs sous forme char*
	
	
	/// server initialization
	
	printf("\nserver initialization, waiting for players to connect...\n\n");
	
	server_t* server = create_server(port_config(config), 1);
	server_accept(server);
	
		/// player number attribution
	server_waitfor(server, "hello");
	server_setbuffer(server, "1");
	server_send(server);
	
	server_waitfor(server, "hello");
	server_setbuffer(server, "2");
	server_send(server);
	
	char* symbcol1 = malloc(2);
	char* symbcol2 = malloc(2);
	strcpy(symbcol1, server_recvfrom(server, '1'));
	server_sendto(server, '1', "received");
	strcpy(symbcol2, server_recvfrom(server, '2'));
	server_sendto(server, '2', "received");
	char* symbcol = malloc(4);
	symbcol[0] = symbcol1[0];
	symbcol[1] = symbcol1[1];
	symbcol[2] = symbcol2[0];
	symbcol[3] = symbcol2[1];
	fill_board(board, symbcol[0], symbcol[2]); // création du board
	
	server_waitfor(server, "1symbcol");
	server_sendto(server, '1', symbcol);
	server_waitfor(server, "1boardsize");
	server_sendto(server, '1', str_board_size);
	server_waitfor(server, "1nbcolor");
	server_sendto(server, '1', str_nb_color);
	server_waitfor(server, "1board");
	server_sendto(server, '1', board_data(board));
	
	printf("\nplayer 1 connected\n\n");
	
	server_waitfor(server, "2symbcol");
	server_sendto(server, '2', symbcol);
	server_waitfor(server, "2boardsize");
	server_sendto(server, '2', str_board_size);
	server_waitfor(server, "2nbcolor");
	server_sendto(server, '2', str_nb_color);
	server_waitfor(server, "2board");
	server_sendto(server, '2', board_data(board));
	
	printf("\nplayer 2 connected\n\n");
	
	/// game
	
	server_setbuffsize(server, 4);
	server_clearbuff(server);
	
	printf("\nstarting game\n\n");
	
	char* curr_move = malloc(4);
	memset(&(curr_move)[0], 0, 5);
	
	while ( (string_equal(server_getbuffer(server), "1win") || string_equal(server_getbuffer(server), "2win")) != 1 )
	{
		server_recv(server);
		if (server_getbuffer(server)[0] == '1')
		{
			if (string_equal(server_getbuffer(server), "1?"))
			{
				if (curr_move[0] == '2')
				{
					server_sendto(server, '1', curr_move + 1);
				} else {
					server_setbuffer(server, "-1");
					server_send(server);
				}
			} else {
				strcpy(curr_move, server_getbuffer(server));
				server_setbuffer(server, "1");
				server_send(server);
			}
		}
		if (server_getbuffer(server)[0] == '2')
		{
			if (string_equal(server_getbuffer(server), "2?"))
			{
				if (curr_move[0] == '1')
				{
					server_sendto(server, '2', curr_move + 1);
				} else {
					server_setbuffer(server, "-1");
					server_send(server);
				}
			} else {
				strcpy(curr_move, server_getbuffer(server));
				server_setbuffer(server, "2");
				server_send(server);
			}
		}
	}
	
	free(curr_move);
	
	server_sendto(server, '1', "end");
	server_sendto(server, '2', "end");
	
	free(symbcol);
	free_config(config);
	free_board(board);
	free_server(server);	
	
	
	return 0;
}

