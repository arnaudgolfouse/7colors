#ifndef BOARD_H
#define BOARD_H

typedef struct board board_t;

board_t* create_board(int size, int nbcolor);
void free_board(board_t* board);
int board_size(board_t* board);
int board_nbcolor(board_t* board);
char* board_data(board_t* board);
void set_cell(board_t* board, int x, int y, char color);
void fill_board(board_t* board, char player1_symbol, char player2_symbol);

#endif /* BOARD_H */
