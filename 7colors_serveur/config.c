#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct config
{
	char P1_color;
	char P2_color;
	int board_size;
	int board_nbcolor;
	int tournament_round;
	int port;
};

void free_config(config_t* config)
{
	free(config);
}

char string_equal(char* str1, char* str2)
{
	unsigned int n;
	n = strlen(str1);
	if (n != strlen(str2))
	{
		return 0;
	}
	for(unsigned int i=0; i < n; i++)
	{
		if (str1[i] != str2[i])
		{
			return 0;
		}
	}
	return 1;
}	

config_t* load_config()
{
	config_t* result = malloc(sizeof(config_t));
	FILE* config_file;
	config_file = fopen("7colors.conf", "r");
	result->P1_color = 255;
	result->P2_color = 255;
	result->board_size = 0;
	result->board_nbcolor = 0;
	result->tournament_round = 1000;
	char* str = malloc(32*sizeof(char));
	while (fscanf(config_file, "%31s", str)  != EOF)
	{
		if (string_equal(str,"P1_color"))
		{
			fscanf(config_file, "%31s", str);
			if (string_equal(str,"="))
			{
				fscanf(config_file, "%31s", str);
				result->P1_color = str[0] - 48;
			}
		}
		if (string_equal(str,"P2_color"))
		{
			fscanf(config_file, "%31s", str);
			if (string_equal(str,"="))
			{
				fscanf(config_file, "%31s", str);
				result->P2_color = str[0] - 48;
			}
		}
		if (string_equal(str,"board_size"))
		{
			fscanf(config_file, "%31s", str);
			if (string_equal(str,"="))
			{
				fscanf(config_file, "%31s", str);
				if (strlen(str) == 2)
				{
					result->board_size = 10 * (str[0] - 48) + str[1] - 48;
				}
			}
		}
		if (string_equal(str,"nb_colors"))
		{
			fscanf(config_file, "%31s", str);
			if (string_equal(str,"="))
			{
				fscanf(config_file, "%31s", str);
				result->board_nbcolor = (int) (str[0] - '0');
			}
		}
		if (string_equal(str,"tournament_round"))
		{
			fscanf(config_file, "%31s", str);
			if (string_equal(str,"="))
			{
				fscanf(config_file, "%31s", str);
				if (strlen(str) == 3)
				{
					result->tournament_round = 100 * (str[0] - 48) + 10 * (str[1] - 48) + str[2] - 48;
				}
			}
		}
		if (string_equal(str, "port"))
		{
			fscanf(config_file, "%31s", str);
			if (string_equal(str, "="))
			{
				fscanf(config_file, "%31s", str);
				result->port = atoi(str);
			}
		}
	}
	free(str);
	fclose(config_file);
	if (result->P1_color > 8)
	{
		printf("Unvalid value for P1_color in 7colors.conf\nUsing default value\n");
		result->P1_color = 1;
	}
	if (result->P2_color > 8)
	{
		printf("Unvalid value for P2_color in 7colors.conf\nUsing default value\n");
		result->P2_color = 2;
	}
	if ((result->board_size > 99) || (result->board_size < 1))
	{
		printf("Unvalid value for board_size in 7colors.conf\nUsing default value\n");
		result->board_size = 30;
	}
	if ((result->board_nbcolor > 8) || (result->board_nbcolor < 1))
	{
		printf("Unvalid value for nb_colors in 7colors.conf\nUsing default value\n");
		result->board_nbcolor = 7;
	}
	if ((result->tournament_round < 0) || (result->tournament_round > 999))
	{
		printf("Unvalid value for tournament_round in 7colors.conf\nUsing default value\n");
		result->tournament_round = 0;
	}
	if (result->P1_color == result->P2_color)
	{
		printf("The two players can't have the same color\nUsing default colors\n");
		result->P1_color = 1;
		result->P2_color = 2;
	}
	return result;
}

char P1_color_config(config_t* config)
{
	return config->P1_color;
}

char P2_color_config(config_t* config)
{
	return config->P2_color;
}

int board_size_config(config_t* config)
{
	return config->board_size;
}

int board_nbcolor_config(config_t* config)
{
	return config->board_nbcolor;
}

int nb_round(config_t* config)
{
	return config->tournament_round;
}

int port_config(config_t* config)
{
	return config->port;
}

void write_default_config()
{
	FILE* config_file;
	config_file = fopen("7colors.conf", "w");
	fclose(config_file);
	remove("7colors.conf");
	config_file = fopen("7colors.conf", "w");
	fprintf(config_file, "P1_color = 1\nP2_color = 2\nboard_size = 30\nnb_colors = 7\ntournament_round = 000\nport = 5000");
	fclose(config_file);
	printf("7colors.conf has been reseted.\nModify it to change the options.\nUse -h or anything else than -r to get the help.\n");
}

void print_help()
{
	printf("You can change the options in the file 7colors.conf\n\nP1_color and P2_color are the colors of each player:\n1 red\n2 blue\n3 green\n4 magenta\n5 cyan\n6 yellow\n7 white\n8 black\n\nboard_size is the size of the board.\n\nnb_color is the number of colors (up to 8).\n\ntournament_round is the number of rounds if you are making an IA tournament, use 000 to turn OFF the tournament mode (the number must be 3 digits long).\n");
}
